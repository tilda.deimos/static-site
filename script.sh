while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        --build-command)
        BUILD_COMMAND="$2"
        shift
        shift
        ;;
        --publish-directory)
        PUBLISH_DIRECTORY="$2"
        shift
        shift
        ;;
        *) # Unknown option
        shift
        ;;
    esac
done

fileExists() (
  excluded_directory="$2"
  find "." -name "$1" -not -path "*/$excluded_directory/*" -print -quit | grep -q "$1"
)

package_manager=
if fileExists "yarn.lock" "node_modules"; then
    package_manager="yarn"
elif fileExists "package-lock.json" "node_modules"; then
    package_manager="npm"
fi

create_dockerfile() {
# Generate Dockerfile content
dockerfile_content=$(cat << EOF
# Use the base image for your chosen package manager
FROM node:14 AS build-stage

WORKDIR /source

# Copy package files and install dependencies
COPY package.json ./
EOF
)

# Conditionally copy package-lock.json or yarn.lock based on package_manager
if [ "$package_manager" == "yarn" ]; then
    dockerfile_content+="\nCOPY yarn.lock ./\n"
else
    dockerfile_content+="\nCOPY package-lock.json ./\n"
fi

dockerfile_content+=$(cat <<EOF
RUN $package_manager install

# Copy the rest of the application code
COPY . .

# Build the static website
RUN ${BUILD_COMMAND}

# Create a minimal image for deployment
FROM nginx:alpine

# Copy built files from the build stage
COPY --from=build-stage /source/${PUBLISH_DIRECTORY} /usr/share/nginx/html

# Expose port 80
EXPOSE 80
EOF
)
echo "$dockerfile_content" > Dockerfile

echo "Dockerfile generated successfully."
}

create_dockerfile